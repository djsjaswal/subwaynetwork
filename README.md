**Subway Excerise**

In order to implement the Subway data model, do the following:

1) Implement a Parser that creates valid SubwayNetwork/Station/Schedule/Edges.

2) Implement SearchInt in your API to implement your functionality.

SearchInt provides the following methods:

* findStationByName(String stationName): Returns the station object given station name.

* canRouteToStation(String startStation, String endStation): Returns a list of stations in order to find route from start station to end station.

* getNextArrival(String Station, Direction direction): Gets arrival given specific station and given direction.

* getArrivalForTime(String station, Direction direction, LocalTime time): Gets arrival time for the station given direction and the time.

* findStationByFeature(String feature): Gets station that provides the feature.

* getLineForStation(String station): Get the line name for a given station.

* getNextStation(String station, Direction direction): Gets the next station given station and direction.

* getPreviousStation(String station, Direction direction): Gets the previous station given the station and direction.