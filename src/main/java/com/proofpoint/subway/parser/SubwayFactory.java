package com.proofpoint.subway.parser;

import com.proofpoint.subway.model.SubwayNetwork;

import java.io.InputStream;

/**
 * Created by Jaswal on 2017-06-18.
 */
public class SubwayFactory {

    /**
     * Builds subway network given the input stream to the input and the incoming input type (csv/xml)
     * @param ios the input stream
     * @param inputType the input type
     * @return the subway network
     */
    public SubwayNetwork buildSubwayNetwork(InputStream ios, InputType inputType) {
        ParserInt parser = null;

        switch(inputType) {
            case CSV:
                parser = new CSVParser();
                break;
            default:
                // ERROR HANDLING HERE
                break;
        }

        return parser.buildSubway(ios);
    }

}
