package com.proofpoint.subway.parser;

import com.proofpoint.subway.model.SubwayNetwork;

import java.io.InputStream;

/**
 * Created by Jaswal on 2017-06-18.
 */
public interface ParserInt {

    SubwayNetwork buildSubway(InputStream ios);

}
