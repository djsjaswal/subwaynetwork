package com.proofpoint.subway.model;

/**
 * Edge defines connections between stations for Subway stations.
 * Created by Jaswal on 2017-06-18.
 */
public class Edge {

    /** The start station of the edge */
    private final SubwayStation startStation;

    /** The end station of the edge */
    private final SubwayStation endStation;

    /** The line name for the edge */
    private final String line;

    /**
     * Constructor for edge to define the start station, the end station and the line the current edge belongs to
     * @param startStation the start station
     * @param endStation the end station
     * @param line the line of the edge
     */
    public Edge(SubwayStation startStation, SubwayStation endStation, String line) {
        this.startStation = startStation;
        this.endStation = endStation;
        this.line = line;
    }

    /**
     * Getter for the start station
     * @return the start station
     */
    public SubwayStation getStartStation() {
        return startStation;
    }

    /**
     * Getter for the end station
     * @return the end station
     */
    public SubwayStation getEndStation() {
        return endStation;
    }

    public String getLine() {
        return line;
    }

}
