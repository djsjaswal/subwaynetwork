package com.proofpoint.subway.model;

/**
 * Directions possible for subway/stop
 * Created by Jaswal on 2017-06-18.
 */
public enum Direction {
    NORTH, SOUTH, EAST, WEST
}
