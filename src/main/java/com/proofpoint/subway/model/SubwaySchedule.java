package com.proofpoint.subway.model;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

/**
 * Subway schedule implementation defining how next arrival/previous arrival actually functions
 * Created by Jaswal on 2017-06-18.
 */
public class SubwaySchedule implements Schedule {

    private Map<DayOfWeek, NavigableSet<LocalTime>> scheduleDates = new TreeMap<>();

    /** The timezone, should be some sort of java property or perhaps an input */
    private static final String TIME_ZONE = "America/Toronto";

    /**
     * Gets the next arrival given date time
     * @param time the date time
     * @return the local time of the next arrival date
     */
    public LocalTime getNextArrival(Date time) {

        NavigableSet<LocalTime> dayTimes = getLocalTimes(time);

        LocalTime localTime = LocalDateTime.ofInstant(time.toInstant(), TimeZone.getTimeZone(TIME_ZONE).toZoneId()).toLocalTime();

        return dayTimes.ceiling(localTime);
    }

    /**
     * Gets the previous arrival given date time
     * @param time the date time
     * @return the local of the previous arrival date
     */
    public LocalTime getPreviousArrival(Date time) {
        NavigableSet<LocalTime> dayTimes = getLocalTimes(time);

        LocalTime localTime = LocalDateTime.ofInstant(time.toInstant(), TimeZone.getTimeZone(TIME_ZONE).toZoneId()).toLocalTime();

        return dayTimes.floor(localTime);
    }

    /**
     * Get local times given date time
     * @param time the time for the incoming date
     * @return the list of schedule times for the give day of week
     */
    private NavigableSet<LocalTime> getLocalTimes(Date time) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(TIME_ZONE));

        cal.setTime(time);
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

        return scheduleDates.get(DayOfWeek.of(dayOfWeek));
    }

    /**
     * Add to schedule given the day of the week and local time
     * @param dayOfWeek the day of the week
     * @param localDateTime the local date time
     */
    public void addToSchedule(DayOfWeek dayOfWeek, LocalTime localDateTime) {
        NavigableSet<LocalTime> localTimes = scheduleDates.get(dayOfWeek);

        if(localTimes == null) {
            localTimes = new TreeSet<>();
        }
        localTimes.add(localDateTime);

        scheduleDates.put(dayOfWeek, localTimes);
    }

}
