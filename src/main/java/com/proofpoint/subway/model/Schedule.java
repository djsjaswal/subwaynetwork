package com.proofpoint.subway.model;

import java.time.LocalTime;
import java.util.Date;

/**
 * Schedule interface
 * Created by Jaswal on 2017-06-18.
 */
public interface Schedule {

    LocalTime getNextArrival(Date datetime);

    LocalTime getPreviousArrival(Date dateTime);
}
