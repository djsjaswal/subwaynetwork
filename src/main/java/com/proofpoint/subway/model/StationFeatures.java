package com.proofpoint.subway.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Station feature defines a map of possible features that might exist for a given station
 * Created by Jaswal on 2017-06-18.
 */
public class StationFeatures {

    private Map<String, Boolean> stationFeature = new HashMap<>();

    public void addFeature(String feature, boolean featureEnabled) {
        stationFeature.put(feature, featureEnabled);
    }

    public Boolean getFeature(String feature) {
        return stationFeature.get(feature);
    }

    public Map<String, Boolean> getAllFeatures() {
        return stationFeature;
    }
}
