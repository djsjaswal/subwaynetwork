package com.proofpoint.subway.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Abstract network can define any network of stations/bus stops as long as there exists a valid name/identifier
 * for the given stop/station
 * Created by Jaswal on 2017-06-18.
 */
public abstract class AbstractNetwork {

    /** The station map maps the name of the station/stop to the abstracted station */
    private Map<String, AbstractStation> stationMap = new HashMap<>();

    /**
     * Add station to the station map object
     * @param station the station object
     */
    public void addStation(AbstractStation station) {
        if(station == null) {
            return;
        }
        stationMap.put(station.getStationName(), station);
    }


    /**
     * Get station object given station name
     * @param stationName the station name
     * @return the station object
     */
    public AbstractStation getStation(String stationName) {
        return stationMap.get(stationName);
    }


    /**
     * Get all the station names for all the lines
     * @return the station name
     */
    public Set<String> getAllStationNames() {
        return stationMap.keySet();
    }


}
