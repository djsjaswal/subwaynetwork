package com.proofpoint.subway.model;

import java.util.*;

/**
 * Defines subway network, each station belongs to a line and a single line consists of many stations
 * Created by Jaswal on 2017-06-18.
 */
public class SubwayNetwork extends AbstractNetwork {

    private Map<String, List<AbstractStation>> lineMapToStation = new HashMap<>();
    /**
     * Adds station to line map
     * @param line the line this station belongs to, one station can belong to multiple lines (intersecting)
     * @param station the station object
     */
    public void addStationToLine(String line, AbstractStation station) {
        List<AbstractStation> stations = lineMapToStation.get(line);

        if(stations == null) {
            stations = new ArrayList<>();
        }

        stations.add(station);

        super.addStation(station);
    }

    /**
     * Get stations for a given line
     * @param lineName the line name
     * @return the list of stations for a particular line
     */
    public List<AbstractStation> getStationsInLine(String lineName) {
        return lineMapToStation.get(lineName);
    }


    /**
     * Get all the lines in the subway network
     * @return the lines in the network
     */
    public Set<String> getLines() {
        return lineMapToStation.keySet();
    }


}
