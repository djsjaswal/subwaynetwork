package com.proofpoint.subway.model;

import java.time.LocalTime;
import java.util.*;

/**
 * Abstract station can define any station/stop that has station name, some schedule and some features (if possible)
 * Created by Jaswal on 2017-06-18.
 */
public abstract class AbstractStation {

    /** The station name */
    private final String stationName;

    /** Some station features */
    private StationFeatures stationFeatures = new StationFeatures();

    /** The schedule for the station/stop given a direction */
    private Map<Direction, Schedule> directionScheduleMap = new EnumMap<>(Direction.class);

    /**
     * Constructor defining the station name
     * @param stationName the station name
     */
    public AbstractStation(String stationName) {
        this.stationName = stationName;
    }

    /**
     * Adds schedule given direction and the schedule itself
     * @param direction the direction of travel
     * @param schedule the schedule object
     */
    public void addSchedule(Direction direction, Schedule schedule) {
        directionScheduleMap.put(direction, schedule);
    }

    /**
     * Returns a schedule for given direction
     * @param direction the direction of travel
     * @return the schedule
     */
    public Schedule getSchedule(Direction direction) {
        return directionScheduleMap.get(direction);
    }


    /**
     * Returns next arrival time at the station given the direction of travel
     * @param directionToTravel the direction of travel
     * @return the next arrival date
     */
    public LocalTime getNextArrival(Direction directionToTravel) {
        Schedule schedule = directionScheduleMap.get(directionToTravel);

        if(schedule == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("ETC"));
        return schedule.getNextArrival(cal.getTime());
    }

    /**
     * Gets arrival time at the station given specific time
     * @param directionToTravel the direction of travel
     * @param departureTime the given departure time
     * @return the next arrival date
     */
    public LocalTime getArrivalForTime(Direction directionToTravel, Date departureTime) {
        Schedule schedule = directionScheduleMap.get(directionToTravel);

        if(schedule == null) {
            return null;
        }

        return schedule.getNextArrival(departureTime);
    }

    /**
     * Getter for the station name
     * @return the station name
     */
    public String getStationName() {
        return this.stationName;
    }


    /**
     * Setter for the station features
     * @param featureName the station features
     * @param featureValue the feature value
     */
    public void addStationFeature(String featureName, boolean featureValue) {
        this.stationFeatures.addFeature(featureName, featureValue);
    }

    /**
     * Getter for the station features
     * @return the stattion features object
     */
    public boolean getStationFeatures(String feature) {
        return stationFeatures.getFeature(feature);
    }

    /**
     * Get all features for a given station
     * @return map of all features
     */
    public Map<String, Boolean> getAllFeatures() {
        return stationFeatures.getAllFeatures();
    }


    @Override
    public boolean equals(Object obj) {
        AbstractStation incomingStation = (AbstractStation)obj;
        return this.stationName.equals(incomingStation.getStationName());
    }

    @Override
    public String toString() {
        return this.stationName;
    }

    @Override
    public int hashCode() {
        return this.stationName.hashCode();
    }
}
