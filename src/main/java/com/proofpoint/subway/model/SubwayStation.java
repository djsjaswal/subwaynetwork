package com.proofpoint.subway.model;

import java.util.Collection;
import java.util.EnumMap;
import java.util.Map;
import java.util.Set;

/**
 * Subway station is a kind of station with subway lines and edges
 * Created by Jaswal on 2017-06-18.
 */
public class SubwayStation extends AbstractStation {

    /** Maps each direction to a specific edge which connects this station to another station */
    private Map<Direction, Edge> directionEdgeEnumMap = new EnumMap<>(Direction.class);

    /**
     * Constructor given the station name
     * @param stationName the station name
     */
    public SubwayStation(String stationName) {
        super(stationName);
    }

    /**
     * Returns all the directions for the given station
     * @return all the directions for the station
     */
    public Set<Direction> getAllDirectionsPossible() {
        return directionEdgeEnumMap.keySet();
    }


    /**
     * Add edge connecting stations given direction
     * @param direction the direction of the edge
     * @param edge the edge connecting stations
     */
    public void addEdge(Direction direction, Edge edge) {
        directionEdgeEnumMap.put(direction, edge);
    }

    /**
     * Returns edge given direction
     * @param direction the direction of travel
     * @return the edge connecting stations
     */
    public Edge getEdge(Direction direction) {
        return directionEdgeEnumMap.get(direction);
    }

    /**
     * Returns all the edges for the given station
     * @return the edges for the station
     */
    public Collection<Edge> getAllEdges() {
        return directionEdgeEnumMap.values();
    }
}
