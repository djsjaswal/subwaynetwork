package com.proofpoint.subway.com.proofpoint.subway.api;

import com.proofpoint.subway.model.Direction;
import com.proofpoint.subway.model.AbstractStation;

import java.time.LocalTime;
import java.util.List;

/**
 * Created by Jaswal on 2017-06-18.
 */
public interface SearchInt {

    AbstractStation findStationByName(String stationName);

    boolean canRouteToStation(String startStation, String endStation);

    boolean canRouteToStation(AbstractStation startStation, AbstractStation endStation);

    LocalTime getNextArrival(AbstractStation startStation, Direction direction);

    LocalTime getNextArrival(String startStation, Direction direction);

    LocalTime getArrivalForTime(AbstractStation startStation, Direction direction, LocalTime time);

    LocalTime getArrivalForTime(String startStation, Direction direction, LocalTime time);

    List<AbstractStation> findStationsByFeature(String feature);

    String getLineForStation(String stationName);

    String getLineForStation(AbstractStation station);

    AbstractStation getNextStation(AbstractStation station, Direction direction);

    AbstractStation getPreviousStation(AbstractStation station, Direction direction);


}
